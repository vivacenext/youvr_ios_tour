//
//  ViewController.swift
//  OSCTest
//
//  Created by vivace on 2017. 8. 30..
//  Copyright © 2017년 vivace. All rights reserved.
//

import UIKit
import OpenSphericalCamera
import Alamofire

class ViewController: UIViewController {
    @IBOutlet var imageView: UIImageView!
    
    //class properties
    var sessionId: String?
    var session: URLSession?
    var stream = httpStream()
    
    var osc: OpenSphericalCamera!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        startRequest()
//
//        print(sessionId)
        
        // Construct OSC generic camera
        osc = OpenSphericalCamera(ipAddress: "192.168.107.1", httpPort: 80)

        self.osc.startSession { (data, response, error) in
            if let data = data , error == nil {
                if let jsonDic = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: Any], let results = jsonDic["results"] as? [String: Any], let sessionId = results["sessionId"] as? String {
                    self.osc.setOptions(sessionId: sessionId, options: ["clientVersion": 2]) { (data, response, error) in

//                        print(data)
//                        print(response)
//                        print(error)
                        
                        print(sessionId)
                        
                        self.osc.takePicture(sessionId: sessionId, progressNeeded: true, completionHandler: { data, response, error in
//                            print(data)
//                            print(response)
//                            print(error)
                            
                            if let data = data , error == nil {
                                let jsonDic = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                                if let jsonDic = jsonDic, let rawState = jsonDic["state"] as? String, let state = OSCCommandState(rawValue: rawState) {
                                    switch state {
                                    case .InProgress:
                                        //assertionFailure()
                                        print("progress")
                                        break
                                    case .Done:
                                        print("done")
                                        print(jsonDic)
                                        if let results = jsonDic["results"] as? NSDictionary, let fileUrl = results["fileUri"] as? String {
                                            
                                            print("aaaa")
                                            
                                            self.osc.getImage(fileUri: fileUrl, completionHandler: { data, response, error in
                                                
                                            })
                                        }
                                    case .Error:
                                        break // TODO
                                    }
                                }
                            }
                        })
                    }
                } else {
                    print("bb")
                    self.startLivePreview()
                    // Assume clientVersion is equal or later than 2
                }
            } else {
                print("cc")
                self.startLivePreview()
            }
        }
    }
    
    @IBAction func startLive(_ sender: Any) {
        startLiveView(block: { frameData in
            print(frameData)
            DispatchQueue.main.async {
                let liveImage = UIImage(data: frameData)
                self.imageView.image = liveImage
            }
        })

    }
    
    func startLivePreview() {
        // Live Preview
        self.osc.getLivePreview { (data, response, error) in
            print(data)
            print(response)
            print(error)
            if let data = data , error == nil {
                
                DispatchQueue.main.async {
                    self.imageView.image = UIImage(data: data)
                }
            }
        }
    }

    func alamoRequest() {
        Alamofire.request("http://192.168.1.1:80/osc/commands/execute", method: .post,
                          parameters: ["name": "camera.startSession", "parameters": [:]],
                          headers: ["Content-Type": "application/json; charset=utf-8"]).responseJSON { response in
                            print("Request: \(String(describing: response.request))")   // original url request
                            print("Response: \(String(describing: response.response))") // http url response
                            print("Result: \(response.result)")                         // response serialization result
                            
                            if let json = response.result.value {
                                print("JSON: \(json)") // serialized json response
                            }
                            
                            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                                print("Data: \(utf8Text)") // original server data as UTF8 string
                            }
        }
    }
    
    func startRequest() {
        var request = createExecuteRequest()
        
        // Create JSON Data
        let body = ["name": "camera.startSession",
                    "parameters": ""]
        let json = try? JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions(rawValue: 0))
        
        request.httpBody = json
        
        // Send the URL Request
        // Init
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 5.0
        session = URLSession(configuration: config)
        
        // Main
        let task = session?.dataTask(with: request, completionHandler: {(data, response, error) in
            
            if(error == nil) {
                let array = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : Any]
                self.sessionId = (array?["results"] as! [String:Any])["sessionId"] as! String
                print(array)
                print(array?["results"])
            }
            else {
                self.sessionId = nil
                print(error)
            }
        })

        task?.resume()
    }
    
    func update() {
        var request = createExecuteRequest()
        
        // Create JSON Data
        let body = ["name": "camera.updateSession",
                    "parameters": ["sessionId":sessionId!]] as [String : Any]
        let json = try? JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions(rawValue: 0))
        
        request.httpBody = json
        
        // Send the URL Request
        // Init
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 5.0
        session = URLSession(configuration: config)
        // Main
        let task = session?.dataTask(with: request, completionHandler: {(data, response, error) in
            var newId: String?
            if error == nil {
                let array = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : Any]
                newId = (array?["results"] as! [String:Any])["sessionId"] as! String
                print(array)
            }
            else {
                self.sessionId = nil
                print(error)
            }
            
            if (newId != nil) {
                self.sessionId = newId
            }
            else {
                self.startRequest()
            }
        })
        
        task?.resume()
    }
    
    func createExecuteRequest() -> URLRequest {
        let server = "192.168.107.1"
        let protocolString = "/osc/commands/execute"
        
        let urlString = "http://" + server + protocolString
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/json; charaset=utf-8", forHTTPHeaderField: "Content-Type")
        
        return request
    }
    
    func connected() -> Bool {
        return (sessionId != nil)
    }
    
    func startLiveView(block: @escaping (_ frameData: Data) -> Void) {
        self.update()
        
        if self.connected() {
            let request = self.createExecuteRequest()
            stream.initWithRequest(request: request)
            stream.setDelegate(bufferBlock: block)
            stream.getData(sessionId: sessionId!)
        }
    }
}
