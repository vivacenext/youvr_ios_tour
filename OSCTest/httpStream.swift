//
//  httpStream.swift
//  OSCTest
//
//  Created by vivace on 2017. 9. 4..
//  Copyright © 2017년 vivace. All rights reserved.
//

import Foundation

typealias CompletionBlock = (Data) -> Void

class httpStream: NSObject {
    let SOI_MARKER: Array = [0xFF, 0xD8]
    let EOI_MARKER: Array = [0xFF, 0xD8]
    
    var request: URLRequest?
    var buffer: Data?
    var frameArray: Array<Any>?
    var task: URLSessionDataTask?
    var isContinue = false
    var onBuffered: CompletionBlock = {_ in }
    
    
    func setDelegate(bufferBlock: @escaping (_ frameData: Data) -> Void) {
        onBuffered = bufferBlock
    }
    
    func initWithRequest(request: URLRequest) {
        self.request = request
        self.buffer = Data()
        self.frameArray = Array()
        isContinue = false
    }
    
    func getData(sessionId: String) {
        if !isContinue {
            let body = ["name": "camera._getLivePreview",
                        "parameters": ["sessionId":sessionId]] as [String : Any]
            let json = try? JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            self.request?.httpBody = json
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
            
            self.task = session.dataTask(with: self.request!)
            task?.resume()
            isContinue = true
        }
    }
    
    func cancel() {
        self.task?.cancel()
    }
}

extension httpStream: URLSessionDataDelegate, URLSessionTaskDelegate {
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        
        
        
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        session.invalidateAndCancel()
        isContinue = false
        print(error)
    }
}
